#!/bin/bash

mkdir "$1 at `Date`"
cd "$1 at `Date`"
mkdir "about me"
cd "about me"
mkdir "personal"
cd "personal"
echo "https://www.facebook.com/$2" > facebook.txt
cd ..
mkdir "professional"
cd "professional"
echo "https://www.linkedin.com/in/$3" > linkedin.txt
cd ..
cd ..
mkdir "my_friends"
cd "my_friends"
curl https://gist.githubusercontent.com/tegarimansyah/e91f335753ab2c7fb12815779677e914/raw/94864388379fecee450fde26e3e73bfb2bcda194/list%2520of%2520my%2520friends.txt -o list_of_my_friends.txt
cd ..
mkdir "my_system_info"
cd "my_system_info"
echo "My username: ilhammuhamadnizar" > about_this_laptop.txt
echo "with host: `uname -a`" >> about_this_laptop.txt
echo "`ping -c 3 -n forcesafesearch.google.com`" > internet_connection.txt
